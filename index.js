const express = require('express');
const fetch = require('node-fetch');

const app = express();
const port = 9191;

const passthroughHeaders = [
  'Accept-Language',
  'Accept-Encoding',
  'Accept',
  'Authorization',
  'Referer',
  'Cookie',
  'User-Agent'
];

const giveHeaders = [
  'Content-Type'
];

const forward = (method) => ((req, res) => {
  const url = decodeURIComponent(req.originalUrl.slice(1));
  const headers = {};
  passthroughHeaders.forEach((header) => {
    const value = req.get(header);

    if (typeof value !== 'undefined') {
      headers[header] = value;
    }
  });
  const start = new Date();
  fetch(url, {
    method,
    headers
  })
  .then((response) => {
    const stop = new Date();
    response.buffer().then((buffer) => {
      console.log(
        start.toISOString(), 'Got', method, 'request for', url,
        'response from server took', (stop.getTime() - start.getTime()) / 1000,
        'seconds and was',
        buffer.length > 1024 ?
          (buffer.length / 1024).toFixed(2) + 'KB' :
          buffer.length + 'B'
      );
      res.set('Access-Control-Allow-Origin', '*');
      giveHeaders.forEach((header) => {
        const value = req.get(header);

        if (typeof value !== 'undefined') {
          res.set(header, value);
        }
      });
      res.status(response.status).send(buffer).end();
    });
  }, (error) => {
    console.log((new Date()).toISOString(), 'got an error');
    res.status(500).end();
  })
});

app.options('*', (req, res) => {
  const url = decodeURIComponent(req.originalUrl.slice(1));
  console.log((new Date()).toISOString(), 'Got OPTIONS request for', url);
  res.set({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Request-Method': 'DELETE, POST, GET, PUT, OPTIONS',
    'Access-Control-Allow-Headers': 'Authorization'
  });
  res.status(200).end();
});
app.get('*', forward('GET'));
app.post('*', forward('POST'));
app.put('*', forward('PUT'));
app.delete('*', forward('DELETE'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));


