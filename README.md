CORS Proxy
==========

Absolute minimum express server for proxying and removing troubles from CORS
while developing.

Don't use this for anything else but testing. Security is there for a reason.

https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
